# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'ayurvedau'
set :deploy_user, 'deploy'

# setup repo details
set :scm, :git
set :repo_url, 'git@bitbucket.org:_SRT_/ayurveda.git'

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# setup rvm.
set :rvm_type, :system                     # Defaults to: :auto
set :rvm_ruby_version, '2.2.1'      # Defaults to: 'default'
# set :rvm_custom_path, '/home/deployer/.rvm/'  # only needed if not detected

# set :rbenv_type, :system
# set :rbenv_ruby, '2.1.1'
# set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
# set :rbenv_map_bins, %w{rake gem bundle ruby rails}

# how many old releases do we want to keep
set :keep_releases, 5

# files we want symlinking to specific entries in shared.
set :linked_files, %w{config/database.yml config/application.yml config/secrets.yml}

# dirs we want symlinking to shared
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# what specs should be run before deployment is allowed to
# continue, see lib/capistrano/tasks/run_tests.cap
set :tests, []

# which config files should be copied by deploy:setup_config
# see documentation in lib/capistrano/tasks/setup_config.cap
# for details of operations
set(:config_files, %w(
  database.example.yml
  application.config.yml
  secrets.example.yml
))

# which config files should be made executable after copying
# by deploy:setup_config
set(:executable_config_files, %w(
  unicorn_init.sh
))

# files which need to be symlinked to other parts of the
# filesystem. For example nginx virtualhosts, log rotation
# init scripts etc.
# set(:symlinks, [
#   {
#     source: "nginx.conf",
#     link: "/etc/nginx/sites-enabled/#{fetch(:full_app_name)}"
#   },
#   {
#     source: "unicorn_init.sh",
#     link: "/etc/init.d/unicorn_#{fetch(:full_app_name)}"
#   }
# ])

# before "deploy",                 "deploy:web:disable"
# before "deploy:stop",            "deploy:web:disable"

# after  "deploy:update_code",     "deploy:symlink_shared"

# after  "deploy:start",           "deploy:web:enable"
# after  "deploy",                 "deploy:web:enable", "deploy:cleanup"

# namespace :deploy do

#   after :restart, :clear_cache do
#     on roles(:web), in: :groups, limit: 3, wait: 10 do
#       # Here we can do anything such as:
#       # within release_path do
#       #   execute :rake, 'cache:clear'
#       # end
#     end
#   end

# end

# namespace :deploy do

  # %w[start stop].each do |command|
  #   desc "#{command} unicorn server"
  #   task command, :roles => :app, :except => { :no_release => true } do
  #     run "#{current_path}/config/unicorn_init.sh #{command}"
  #   end
  # end

  # desc "restart unicorn server"
  # task :restart, :roles => :app, :except => { :no_release => true } do
  #   run "#{current_path}/config/unicorn_init.sh upgrade"
  # end


  # desc "Link in the production database.yml and assets"
  # task :symlink_shared do
  #   run "ln -nfs #{deploy_to}/shared/config/database.yml #{release_path}/config/database.yml"
  # end


  # namespace :web do
  #   desc "Maintenance start"
  #   task :disable, :roles => :web do
  #     on_rollback { run "rm #{shared_path}/system/maintenance.html" }
  #     page = File.read("public/503.html")
  #     put page, "#{shared_path}/system/maintenance.html", :mode => 0644
  #   end
    
  #   desc "Maintenance stop"
  #   task :enable, :roles => :web do
  #     run "rm #{shared_path}/system/maintenance.html"
  #   end
  # end

# end