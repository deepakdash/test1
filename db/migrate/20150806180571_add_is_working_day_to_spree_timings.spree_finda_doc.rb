# This migration comes from spree_finda_doc (originally 20150203210501)
class AddIsWorkingDayToSpreeTimings < ActiveRecord::Migration
  def change
    add_column :spree_timings, :is_working_day, :boolean, default: false
  end
end
