# This migration comes from spree_finda_doc (originally 20150112172513)
class CreateSpreeCities < ActiveRecord::Migration
  def change
    create_table :spree_cities do |t|
      t.string :name

      t.timestamps
    end

    # run 'bundle exec rake db:seed_geoobjects'
  end
end
