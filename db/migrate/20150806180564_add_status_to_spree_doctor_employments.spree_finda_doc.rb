# This migration comes from spree_finda_doc (originally 20150129195603)
class AddStatusToSpreeDoctorEmployments < ActiveRecord::Migration
  def change
    add_column :spree_doctor_employments, :status, :integer
  end
end
