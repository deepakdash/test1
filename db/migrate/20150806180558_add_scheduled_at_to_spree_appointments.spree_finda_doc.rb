# This migration comes from spree_finda_doc (originally 20150120115401)
class AddScheduledAtToSpreeAppointments < ActiveRecord::Migration
  def change
    add_column :spree_appointments, :scheduled_at, :datetime
  end
end
