# This migration comes from spree_finda_doc (originally 20150113080107)
class AddNameToSpreeClinics < ActiveRecord::Migration
  def change
    add_column :spree_clinics, :name, :text
  end
end
