# This migration comes from spree_finda_doc (originally 20150207183030)
class RenameClinicsServices < ActiveRecord::Migration
  def change
    rename_table :clinics_services, :spree_clinics_services
  end
end
