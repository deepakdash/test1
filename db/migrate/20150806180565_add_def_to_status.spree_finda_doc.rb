# This migration comes from spree_finda_doc (originally 20150129202751)
class AddDefToStatus < ActiveRecord::Migration
  def change
    change_column :spree_doctor_employments, :status, :integer, default: 0
  end
end
