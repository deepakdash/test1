# This migration comes from spree_finda_doc (originally 20150203124214)
class AddPhoneIsVerifiedToSpreeDoctors < ActiveRecord::Migration
  def change
    add_column :spree_doctors, :phone_is_verified, :boolean
  end
end
