# This migration comes from spree_finda_doc (originally 20150120161140)
class AddAppointmentToSpreeReviews < ActiveRecord::Migration
  def change
    add_reference :spree_reviews, :appointment, index: true
  end
end
