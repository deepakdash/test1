# This migration comes from spree_finda_doc (originally 20150208182534)
class AddAutoConfirmableToSpreeDoctors < ActiveRecord::Migration
  def change
    add_column :spree_doctors, :auto_confirmable, :boolean, default: true
  end
end
