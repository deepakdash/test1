# This migration comes from spree_finda_doc (originally 20150127215810)
class AddSlugToSpreeDoctors < ActiveRecord::Migration
  def change
    add_column :spree_doctors, :slug, :string
    add_index :spree_doctors, :slug, unique: true
  end
end
