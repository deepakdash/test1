# This migration comes from spree_finda_doc (originally 20150204202318)
class ChangeIsWorkingDayToInt < ActiveRecord::Migration
  def change
    remove_column :spree_timings, :is_working_day
    add_column :spree_timings, :is_working_day, :integer, default: 0
  end
end
