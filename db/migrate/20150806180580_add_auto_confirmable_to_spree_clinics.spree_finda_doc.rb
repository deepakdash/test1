# This migration comes from spree_finda_doc (originally 20150225180110)
class AddAutoConfirmableToSpreeClinics < ActiveRecord::Migration
  def change
    add_column :spree_clinics, :auto_confirmable, :boolean, default: true
  end
end
