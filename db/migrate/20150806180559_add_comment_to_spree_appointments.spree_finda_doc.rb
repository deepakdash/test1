# This migration comes from spree_finda_doc (originally 20150120123644)
class AddCommentToSpreeAppointments < ActiveRecord::Migration
  def change
    add_column :spree_appointments, :comment, :text
  end
end
