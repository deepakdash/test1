# This migration comes from spree_finda_doc (originally 20150120103125)
class AddDegreeToSpreeDoctors < ActiveRecord::Migration
  def change
    add_column :spree_doctors, :degree, :string
  end
end
