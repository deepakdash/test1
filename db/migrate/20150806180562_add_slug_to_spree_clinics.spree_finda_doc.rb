# This migration comes from spree_finda_doc (originally 20150127215754)
class AddSlugToSpreeClinics < ActiveRecord::Migration
  def change
    add_column :spree_clinics, :slug, :string
    add_index :spree_clinics, :slug, unique: true
  end
end
